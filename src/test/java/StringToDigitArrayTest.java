import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawelbryniarski on 26.03.16.
 */
@RunWith(Parameterized.class)
public class StringToDigitArrayTest {

    private final String input;
    private final int[] output;
    private StringToDigitArray stringToDigitArray;

    public StringToDigitArrayTest(String input, int[] output) {
        this.input = input;
        this.output = output;
    }

    @Parameterized.Parameters
    public static Collection inputsOutputs() {
        return Arrays.asList(new Object[][]{
                {"123456789", new int[]{1,2,3,4,5,6,7,8,9}},
                {"123456789123456789", new int[]{1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9}},
                {"00000", new int[]{0,0,0,0,0}}
        });
    }

    @Before
    public void setUp() throws Exception {
        stringToDigitArray = new StringToDigitArray();
    }

    @Test
    public void testRead() throws Exception {
        int [] computedOutput = stringToDigitArray.convert(input);
        assertEquals(output.length, computedOutput.length);
        for (int i = 0; i < output.length; i++) {
            assertEquals(output[i], computedOutput[i]);
        }
    }
}