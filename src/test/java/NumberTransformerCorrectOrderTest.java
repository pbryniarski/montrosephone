import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by pawelbryniarski on 03.04.16.
 */
public class NumberTransformerCorrectOrderTest {

    private NumberTransformer transformer;

    @Before
    public void setUp() throws Exception {
        transformer = new NumberTransformer();
    }

    @Test
    public void testCorrectOrder() throws Exception {

        List<String> transformed = transformer.getTransforms(new int[]{1,2,3,4,5,6,7,8,9,0});
        assertTrue("Transformed list is sorted", isSorted(transformed));
    }

    private boolean isSorted(List<String> list){
        Iterator<String> iter = list.iterator();
        String s1 = iter.next();
        while (iter.hasNext()) {
            String s2 = iter.next();
            if (s1.compareTo(s2) > 0) {
                return false;
            }
            s1 = s2;
        }
        return true;
    }
}
