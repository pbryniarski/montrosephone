import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawelbryniarski on 26.03.16.
 */
@RunWith(Parameterized.class)
public class InputReaderTest {

    private InputReader reader;
    private final String input;
    private final String output;

    public InputReaderTest(String input, String output) {
        this.input = input;
        this.output = output;
    }


    @Parameterized.Parameters
    public static Collection inputsOutputs() {
        return Arrays.asList(new Object[][]{
                {"123456789", "123456789"},
                {"999999999", "999999999"},
                {"123456&89", "123456&89"},
                {"1238", "1238"},
                {"" + System.lineSeparator(), ""},
                {"12 " + System.lineSeparator() + "1234", "12 "},
                {"123456789" + System.lineSeparator() + "1234", "123456789"},
                {"         ", "         "}
        });
    }

    @Before
    public void setUp() throws Exception {
        InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        reader = new InputReader(stream);
    }

    @Test
    public void testRead() throws Exception {
        assertEquals(output, reader.read());
    }

}