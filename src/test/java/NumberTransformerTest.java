import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawelbryniarski on 26.03.16.
 */
@RunWith(Parameterized.class)
public class NumberTransformerTest {

    private static final String[] output1 = new String[]{"11111111W", "11111111X", "11111111Y"};
    private static final String[] output2 = new String[]{"00000001A", "00000001B", "00000001C"};
    private static final String[] output3 = new String[]{"1W0001010", "1X0001010", "1Y0001010"};
    private final int[] input;
    private final List<String> output;
    private NumberTransformer transformer;

    public NumberTransformerTest(int[] input, List<String> output ) {
        this.input = input;
        this.output = output;
    }

    @Parameterized.Parameters
    public static Collection inputsOutputs() {
        return Arrays.asList(new Object[][]{
                {new int[]{1,1,1,1,1,1,1,1,9}, Arrays.asList(output1)},
                {new int[] {0,0,0,0,0,0,0,1,2},Arrays.asList(output2)},
                {new int[] {1,9,0,0,0,1,0,1,0}, Arrays.asList(output3)}
        });
    }

    @Before
    public void setUp() throws Exception {
        transformer = new NumberTransformer();
    }

    @Test
    public void testRead() throws Exception {
        List<String> computed = transformer.getTransforms(input);
        assertEquals(output, computed);
    }
}