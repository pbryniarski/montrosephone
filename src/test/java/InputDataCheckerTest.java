import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawelbryniarski on 26.03.16.
 */
@RunWith(Parameterized.class)
public class InputDataCheckerTest {

    private final String phoneNumber;
    private final Boolean isValid;
    private InputDataChecker checker;

    public InputDataCheckerTest(String  phoneNumber,
                                  Boolean isValid) {
        this.phoneNumber = phoneNumber;
        this.isValid = isValid;
    }

    @Parameterized.Parameters
    public static Collection inputsOutputs() {
        return Arrays.asList(new Object[][]{
                {"123456789", true},
                {"999999999", true},
                {"123456&89", false},
                {"12345678", false},
                {"", false},
                {null, false},
                {"****@!",false}
        });
    }

    @Before
    public void setUp() throws Exception {
        checker = new InputDataChecker();
    }

    @Test
    public void testIsValidPhoneNumber() throws Exception {
        assertEquals(isValid, checker.isValidPhoneNumber(phoneNumber));
    }
}