/**
 * Created by pawelbryniarski on 26.03.16.
 */
public final class InputDataChecker {

    boolean isValidPhoneNumber(String phoneNumber) {

        if (isNull(phoneNumber) || isInvalidLength(phoneNumber) || containsIncorrectCharacters(phoneNumber)) {
            return false;
        }

        return true;
    }

    private boolean isNull(String phoneNumber) {
        return phoneNumber == null;

    }

    private boolean isInvalidLength(String phoneNumber) {
        return phoneNumber.length() != 9;
    }

    private boolean containsIncorrectCharacters(String phoneNumber) {
        for (int i = 0; i < 9; i++) {
            if (!Character.isDigit(phoneNumber.charAt(i))) {
                return true;
            }
        }
        return false;
    }

}
