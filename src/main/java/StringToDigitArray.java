/**
 * Created by pawelbryniarski on 31.03.16.
 */
public class StringToDigitArray {

    public int[] convert(String number){
        int [] digits = new int[number.length()];
        for (int i = 0; i < number.length(); i++) {
                digits[i] = Character.getNumericValue(number.charAt(i));
        }
        return digits;
    }
}
