import java.util.ArrayList;
import java.util.List;

/**
 * Created by pawelbryniarski on 31.03.16.
 */
public class NumberTransformer {

    //There are 10 digits 0-9
    private static final int NR_OF_DIGITS = 10;
    // A key can be mapped to 3 letters
    private static final int NR_OF_LETTERS = 3;

    private static final int PHONE_NUMBER_LENGTH = 9;
    private static final int maxDepth = PHONE_NUMBER_LENGTH - 1;
    private static final char[][] transformations = new char[NR_OF_DIGITS][3];

    public NumberTransformer() {
        transformations[0] = new char[]{'0', '0', '0'};
        transformations[1] = new char[]{'1', '1', '1'};
        transformations[2] = new char[]{'A', 'B', 'C'};
        transformations[3] = new char[]{'D', 'E', 'F'};
        transformations[4] = new char[]{'G', 'H', 'I'};
        transformations[5] = new char[]{'J', 'K', 'L'};
        transformations[6] = new char[]{'M', 'N', 'O'};
        transformations[7] = new char[]{'P', 'R', 'S'};
        transformations[8] = new char[]{'T', 'U', 'V'};
        transformations[9] = new char[]{'W', 'X', 'Y'};
    }

    public List<String> getTransforms(int number[]) {
        List<String> transformedNumbers = new ArrayList<>();
        combine(number, 0, "", transformedNumbers);
        return transformedNumbers;
    }

    private static void combine(int[] number, int depth, String currentString, List<String> output) {
        if (depth > maxDepth) {
            output.add(currentString);
            return;
        }

        if (number[depth] == 0 || number[depth] == 1) {
            combine(number,depth + 1, currentString + transformations[number[depth]][0], output);

        } else {
            for (int i = 0; i < NR_OF_LETTERS; i++) {
                combine(number, depth + 1, currentString + transformations[number[depth]][i], output);
            }
        }
    }
}
