import java.util.List;

/**
 * Created by pawelbryniarski on 26.03.16.
 */
public class Main {

    public static void main(String[] args) {

        InputReader reader = new InputReader(System.in);
        String phoneNumber = reader.read();

        InputDataChecker checker = new InputDataChecker();

        if (!checker.isValidPhoneNumber(phoneNumber)) {
            System.out.print("ERROR");
            return;
        }

        StringToDigitArray converter = new StringToDigitArray();

        NumberTransformer transformer = new NumberTransformer();

        List<String> transformations = transformer.getTransforms(converter.convert(phoneNumber));

        for (String transformation : transformations) {
            System.out.println(transformation);
        }
    }
}