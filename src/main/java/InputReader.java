import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by pawelbryniarski on 26.03.16.
 */
public class InputReader {

    private final InputStream inputStream;

    public InputReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String read() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            return br.readLine();
        } catch (IOException io) {
            io.printStackTrace();
            return null;
        }
    }
}
